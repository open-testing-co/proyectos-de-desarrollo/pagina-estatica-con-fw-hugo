---
title: PROFILE
description: Henry Correa professional profile
images: ["/images/handres.jpg"]
---

Hey,

I'm a Software Development Engineer in Testing and have been working on software testing for 8 years, my experience as a QA Automation is creating testing frameworks from scratch, choosing the best testing approach, ensuring quality, continuous testing, using agile frameworks, and team-leading. I have a bachelor’s degree in electronic engineering and work with programming languages such as JAVA, C#, JavaScript, Python, Ruby, C++, Gherkin, and Visual Basic. I know several automation testing tools like Selenium, WebDriver, Cucumber, REST Assured, Selenium Grid, Cypress, Playwright, JMeter, Jasmin, Serenity BDD, Appium, Jbehave, SonarQube, Postman, Autoit, SilkTest, Protractor, TestNG, Nunit, Junit, SQL, MongoDB. I've been working with CI/CD tools such as Azure DevOps, AWS, Google Cloud, Docker, Gitlab CI, Travis, Jenkins, GitHub Actions, and Kubernetes. Additionally, I have been working with different kinds of industries like Fintech, Retail, Telecommunications, Health Care, Oil & Gas, and E-Commerce.

[Get to know me better](./experience "Get to know me better")