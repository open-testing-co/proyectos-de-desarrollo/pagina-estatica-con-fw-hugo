---
title: EXPERIENCE
description: Henry Correa Professional Experience
images: ["/images/handres.jpg"]
---


This is my professional experience. :wave: :smile:
   

### Software Engineer in Test | SDETSoftware Engineer in Test | SDET
&nbsp;&nbsp;&nbsp;&nbsp;**Sniffle AI** \
&nbsp;&nbsp;&nbsp;&nbsp;*Jan 2023 - Present* \
&nbsp;&nbsp;&nbsp;&nbsp;*Remote*
- Creating and executing automated tests using frameworks such as Flutter, Selenium. 
- Using programming languages such as Java, JavaScript, C#, and Dart to write test scripts. 
- Performing code analysis and debugging using tools such as SonarQube, Visual Studio Code, and Chrome DevTools. 
- Using CI/CD tools such as Jenkins, Azure DevOps, GitHub Actions, Firebase, and Codemagic to automate the testing process and deliver quality software. 
- Using Postman, Datalog, and SQL to test APIs and databases.
- Designing strategies to improve the quality in the company in a short, mid and long terms. 
- Leading QA team and mentoring junior testers. 
- Using Jira, Confluence, TestRails, and Agile methodologies to manage testing projects and communicate with stakeholders.


### Software Development Engineer in Test (SDET)
&nbsp;&nbsp;&nbsp;&nbsp;**Sniffle AI** \
&nbsp;&nbsp;&nbsp;&nbsp;*Aug 2022 – Jun 2022 (1 year)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Remotely*
- Developed AI-based assistants to enhance user experience in telemedicine.
- Conducted comprehensive testing of user flows, including onboarding, booking appointments, and finding providers.
- Ensured HIPAA compliance certification for the platform.
- Created and maintained automated tests using Flutter integration tests.
- Utilized MongoDB, PostgreSQL, and Postman for database management and API testing.
- Led the QA team and oversaw all quality assurance activities.
- Ran static code analysis to maintain code quality.


### Software Engineer in Test | SDETSoftware Engineer in Test | SDET
&nbsp;&nbsp;&nbsp;&nbsp;**PayPal · Full-timePayPal · Full-time** \
&nbsp;&nbsp;&nbsp;&nbsp;*Dec 2021 - Jan 2023  (1 year and 2 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*San Jose, CA, USA - Remote*
- Designing test automation in the PayPal client's on-boarding projects by establishing, monitoring and maintaining the existing automated tests. 
- Identifying and troubleshooting issues with the automated tests. 
- Improving and designing in a more efficient way the existing automated tests by adaption best practices and design patterns like Page Object Model, Screenplay, Driver Factory and best strategies for Element locators.
- Understanding the needs of the product to advise on and design testing strategies. 
- Compiling and reporting results in an automated mode and in real-time.
- Performing regression and performance testing in order to give my sign-off for every release.
- Tracking bugs and documenting features in new releases.
- Using Jenkins, Selenium, Nemo (by PayPal), Cypress, Playwright, JavaScript, CI/CD jobs, Postman, SQL, TestRails, -Jira, Confluence, and Agile methodologies


### Senior QA Automation Engineer
&nbsp;&nbsp;&nbsp;&nbsp;**TEAM International** \
&nbsp;&nbsp;&nbsp;&nbsp;*Aug 2019 – Dec 2021 (2 year and 5 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellin, Colombia*
- Microsoft Platform (.NET, ASP.NET, C#, C/C++ etc.)
- Web Development (HTML5, HTML, Flash/Flex, PHP/Pythonetc.)
- Java / Open Source (Java EE, Java ME etc.)
- Mobile / Smartphone Platforms (iOS/Objective-C, Google
- Android/Java, Windows Mobile/.NET)
- TEAM has served over 60 global clients and completed over 100 projects for global clients in a number of niches including ISV, HR, Telecommunication, Transportation, Leisure and Entertainment, Financial, e-Commerce.


### Test Automation Engineer
&nbsp;&nbsp;&nbsp;&nbsp;**Indra** \
&nbsp;&nbsp;&nbsp;&nbsp;*Aug 2018 – Aug 2019 (1 year and 1 month)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellin, Colombia* \
Test automation team leader in charge of the continuous integration testing of several customer applications which consists of test versioning, remote execution, evidence publication, application source code analyst, notification of reports, and make and run Pipeline in multiple environments. Jenkins - Selenium - Cucumber - Serenity BDD - SonarQuebe - UnitTests - Continuous Integration - Continuous Deployment - Continuous Testing.

### Test Automation Engineer
&nbsp;&nbsp;&nbsp;&nbsp;**Choucair Testing S.A.** \
&nbsp;&nbsp;&nbsp;&nbsp;*Jul 2017 – Aug 2018 (1 year and 2 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellin, Colombia* \
Test automation engineer who development and maintenance of the testing framework, test project leader, daily, weekly and monthly reporting, the suite of applications, tests throughout the software development life cycle, migration tests, requirements tests, End to End tests, general tests. \
Participation in a billing software development project with integrations of other systems, integrations, administration and credit management software, customer loyalty, transactional systems, client management, Web architectures, Client - Server, Excel macros, tool management Web and Desktop application automation, self-management, effective communication, result orientation, software architecture, agile methodologies, scrum, Kanban, work cells for complex software projects, risk-based tests, Pareto, case design techniques test.

### Software Development Engineer
&nbsp;&nbsp;&nbsp;&nbsp;**Smartchip SAS** \
&nbsp;&nbsp;&nbsp;&nbsp;*Feb 2016 – Jul 2017 (1 year and 6 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellin, Colombia* \
Development applications and customer requirements in high-level languages such as JAVA, .NET, and SQL. Application solution RFID with smart cards, hardware, and card readers, meet customer requirements, migration of low-level language applications to high-level language programming, estimating the required hardware according to application requirements, database administration, web services, and websites.

### Junior Electronic Engineer
&nbsp;&nbsp;&nbsp;&nbsp;**Línea Directa S.A.S** \
&nbsp;&nbsp;&nbsp;&nbsp;*Jul 2015 – Feb 2016 (8 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*La estella, Antioquia, Colombia* \
Creation and updating of equipment datasheets; supplies and spare parts inventory control and creation; preparation of papers for supplies and spare parts orders; quotation of supplies and spare parts; creation of electrical diagrams of the buildings; calculation of pneumatic system and electric circuit consumptions; dimensioning of new electricity circuits and pneumatic system after the acquisition of new equipment; maintenance protocols update; planning of preventive maintenance of equipment in a factory; execution of preventive and corrective maintenance; support in the creation of budgets for equipment maintenance.

### Fascilitador Innobotica
&nbsp;&nbsp;&nbsp;&nbsp;**Pygmalion Colombia** \
&nbsp;&nbsp;&nbsp;&nbsp;*Mar 2014 – Nov 2014 (9 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellín* \
Impartir clases de robótica, electrónica, programación computacional, a jóvenes de instalaciones educativas, llenar formatos de asistencia, apoyar el diseño de sesiones, investigar sobre nuevas aplicaciones, tecnología, métodos e innovaciones que puedan ser aplicadas al proyecto innobótica, impartir mini conferencias a niños y jóvenes en festivales y ferias.

### Monitor Lógica y Programación
&nbsp;&nbsp;&nbsp;&nbsp;**Universidad de Antioquía** \
&nbsp;&nbsp;&nbsp;&nbsp;*Jan 2012 – Dec 2013 (2 years)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellín, Colombia* \
Brindar asesoría a estudiantes universitarios, comunicar asertivamente errores y fallas, diseño de prácticas de laboratorio, diseñar y realizar talleres que faciliten la programación computacional, reportar notas finales del laboratorio de programación, detectar fallas y errores en códigos fuente y archivos ejecutables, realizar ejemplos y librerías en C++ que faciliten la comprensión de la programación computacional.


### Auxiliar Administrativo
&nbsp;&nbsp;&nbsp;&nbsp;**Universidad de Antioquia** \
&nbsp;&nbsp;&nbsp;&nbsp;*Mar 2011 – Sep 2011 (7 months)* \
&nbsp;&nbsp;&nbsp;&nbsp;*Medellín, Colombia* \
Crear evidencia de procedimientos realizados en el departamento de formación académica, apoyar el proceso de contratación de docentes, apoyar requerimiento de equipos de docentes y laboratoristas.