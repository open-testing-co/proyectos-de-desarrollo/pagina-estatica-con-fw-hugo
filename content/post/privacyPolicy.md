---
title: Privacy Policies
description: Privacy policies
date: "2024-05-02T19:49:05+02:00"
publishDate: "2024-05-02T19:49:05+02:00"
---

No mobile information will be shared with third parties/affiliates for marketing/promotional purposes. All other categories exclude text messaging originator opt-in data and consent; this information will not be shared with any third parties...

<!--more-->
# Privacy Policy

## Who We Are 
The address of our website is: https://open-testing-co.gitlab.io/proyectos-de-desarrollo/pagina-estatica-con-fw-hugo/.

## What Personal Data We Collect and Why We Collect It
Our users do not create a profile, therefore they cannot upload profile pictures or post personal information. The only information we collect from our clients is the information provided by them in emails or calls. We do not ask personal questions nor do we share this information with third parties.

## What Rights You Have Over Your Data 
Regarding the provided data, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us or we have generated about you. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.

## Where We Send Your Data 
Visitor comments may be checked through an automated spam detection service.

## Your Contact Information

### Additional Information
#### How We Protect Your Data 
We implement various security measures to maintain the safety of your personal information.

### What Data Breach Procedures We Have In Place 
We have a comprehensive data breach response plan that includes notifying affected users and regulatory authorities as required by law.
### What Third Parties We Receive Data From 
We may receive data about you from third-party services that help us identify and prevent fraud.
What Automated Decision Making and/or Profiling We Do with User Data We do not perform automated decision making or profiling with user data.

# Industry Regulatory Disclosure Requirements - SMS

## Privacy Policy for SMS Campaign 
No mobile information will be shared with third parties or affiliates for marketing or promotional purposes. All other categories exclude text messaging originator opt-in data and consent; this information will be used solely for responding to client inquiries and will not be shared with any third parties.

## SMS Disclosure 
By providing a telephone number or contact me, you consent to be contacted by SMS text message. Message and data rates may apply. You can reply ***STOP*** to opt-out of further messaging and get more help by sending ***HELP***.