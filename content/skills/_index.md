---
title: SKILLS
description: 'Skills'
---

# Tech Skills
## Web
- Cucumber
- Selenium
- Selenium WebDriver
- Selenium Grid
- Selenium IDE
- Cypress
- WebDriver.IO
## APIs
- RestAssured
- JMetter
- Gatling
- SoapUI
- Postman
## Mobile
- Appium
- IOS
- Android
- UIAutomation2
- XCUITest
## DevOps
- Jenkins
- Azure DevOps
- Docker
- Kubernetes
- Google Cloud
- AWS
- Pipelines
- YML
## Languajes
- Java
- C#
- JavaScript
- Ruby
- Python
## Techiques
- BDD
- TDD
- Page Object Model
- Screenplay
- Scripting
- Keyword Frameworks
- Record and Playback

# Soft Skills
- Focus
- Empathy
- Leadership
- Confidence
- Self-management
- Common sense
- Situational awareness
- Enthusiasm
- Conflict resolution
- Assertiveness
- Positive Attitude
- Taking Responsibility